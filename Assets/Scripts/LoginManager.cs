﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LoginManager : MonoBehaviour {


	public InputField InputField_UserName ;
	public InputField InputField_Email ;
	public InputField InputField_PhoneNumber ;
	public Toggle isReadyToAccept;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onContinueButtonClick()
	{
		if (InputField_UserName.text.Length < 0) {
			return;
		}
		if (InputField_Email.text.Length < 0) {
			return;
		}
		if (InputField_PhoneNumber.text.Length < 0) {
			return;
		}

		StartCoroutine (SignIn(InputField_UserName.text,InputField_Email.text,InputField_PhoneNumber.text));
	//	SignIn (InputField_UserName.text,InputField_Email.text,InputField_PhoneNumber.text);

	}


	IEnumerator SignIn(string name, string email, string password)
	{
		WWWForm form = new WWWForm();
		form.AddField("name", name);
		form.AddField("email", email);
		form.AddField("password", password);

		using (UnityWebRequest www = UnityWebRequest.Post("http://arapp.stora2.co/index.php/site/new", form))
		{
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
			}
			else
			{
				PlayerPrefs.SetInt ("isloggedin", 1);
				Debug.Log("Form upload complete!");
			}
		}
	}
}
