﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class AnalyticsManager : MonoBehaviour {



	public static AnalyticsManager Instance = null;
	public GoogleAnalyticsV4 googleAnalytics;


	void Awake () {
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}


	void Start () {

		FacebookInit ();
		googleAnalytics.initialise ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void FacebookInit () {
		if (!FB.IsInitialized) {
			// Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}
	}
	private void InitCallback ()
	{
		if (FB.IsInitialized) {
			FB.ActivateApp();
		} else {
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}
	public void logEvent(string eventCategory, string eventAction,string eventLabel, long value)
	{
//		
	}


	public void logGenericEvent (string eventCategory, string eventName, string parameterName, string value) {
		var parameters = new Dictionary<string, object>();
		parameters[parameterName] = value;
		FB.LogAppEvent(
			eventName,0f,
			parameters
		);

		googleAnalytics.LogEvent (eventCategory,eventName,parameterName,0);

	}
}
