﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {


	public GameObject registration_Panel;
	// Use this for initialization
	void Start () {
		

		if (PlayerPrefs.GetInt ("isloggedin", 0) == 0) {
			registration_Panel.SetActive (true);
		} else {
			registration_Panel.SetActive (false);
		}
		
	}

	void Update () {
		
	}

	public void onContinueButtonClick()
	{
		registration_Panel.SetActive (false);
	}
}
