﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Video;
using WPLitJson;

public class VideoManager : MonoBehaviour {
    public static VideoManager Instance;

    public VideoPlayer videoPlayer;
    public GameObject objLoading;
	public GameObject objButtons;
	public WebViewHandler webView;

    WWW www;
    string sURL = "https://www.dropbox.com/s/w59wy3dzziq4i8v/videoplayback.mp4?dl=1";
    string sPath;

    private string sEmail = "";
    private string sPhone = "";
    private string sWWW = "";

    private void Awake()
    {
        Instance = this;
    }

    void Start () {
        videoPlayer.prepareCompleted += PrepareCompleted;
	}

    public void DownloadVideo(){
        StartCoroutine(DownloadCo());
    }

    public void PlayVideo(){
        Debug.Log("StartPlay");
        videoPlayer.url = sPath;
        Debug.Log("Saved Path\t"+sPath);
        videoPlayer.Play();
    }

    public void PlayVideoCloud(string url)
    {
        Debug.LogError("URL..."+url);

        JsonData a = JsonMapper.ToObject(url);

        sEmail  = a["email"].ToString();
        sPhone  = a["phone"].ToString();
        sWWW    = a["website"].ToString();

        videoPlayer.url = a["video_url"].ToString();
        videoPlayer.Prepare();
        objLoading.SetActive(true);
    }

    void PrepareCompleted(VideoPlayer vp)
    {
        Debug.Log("On Prepare");
        objLoading.SetActive(false);
        vp.Play();
    }


    IEnumerator DownloadCo(){
        Debug.Log("StartDownload");
        www = WWW.LoadFromCacheOrDownload(sURL, 1);
        yield return www;

        Debug.Log("Finish Downalod");

        sPath = Application.persistentDataPath + "/" + "vijay.mp4";
        File.WriteAllBytes(sPath, www.bytes);

        Debug.Log("Saved");
    }

    public void BtnCall(){
        Debug.Log("Call");
        Application.OpenURL("tel://"+sPhone);
    }

    public void BtnEmail(){
        Debug.Log("Email");

        string email    = sEmail;
        string subject  = MyEscapeURL("My Subject");
        string body     = MyEscapeURL("My Body\r\nFull of non-escaped chars");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    public void BtnWWW(){
        Debug.Log("WWW");
        if(!sWWW.Contains("https://www.")){
            sWWW = "https://www." + sWWW;
        }

		webView.loadURL (sWWW);
//        Application.OpenURL(sWWW);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void OnTrackinLost(){
        objButtons.SetActive(false);
        videoPlayer.Stop();
    }

    public void OnTrackingFind(){
        objButtons.SetActive(true);
    }
}