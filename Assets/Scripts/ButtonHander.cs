﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonHander : MonoBehaviour {

	public UnityEvent OnClick = new UnityEvent();
	public VideoManager videoManager;
	public SpriteRenderer spriteRenderer;
	public enum ButtonType{
		phoneLink,
		emailLink,
		webLink
	}




	public ButtonType buttonType;

	void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	void OnMouseDown(){

		if (spriteRenderer) {
			spriteRenderer.color = Color.gray;
		}


		Debug.Log ("On Click");
		OnClick.Invoke();


		if (buttonType == ButtonType.phoneLink) {
			videoManager.BtnCall ();
		}
		else if (buttonType == ButtonType.emailLink) {
			videoManager.BtnEmail ();

		}
		else if (buttonType == ButtonType.webLink) {

			videoManager.BtnWWW ();

		}

	}

	void OnMouseUp()
	{
		if (spriteRenderer) {
			spriteRenderer.color = Color.white;
		}

	}
}

/*
 <?xml version="1.0" encoding="utf-8"?>
<manifest
    xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.onevcat.uniwebview"
    xmlns:tools="http://schemas.android.com/tools"
    android:installLocation="preferExternal"
    android:versionCode="1"
    android:versionName="1.0">
    <supports-screens
        android:smallScreens="true"
        android:normalScreens="true"
        android:largeScreens="true"
        android:xlargeScreens="true"
        android:anyDensity="true"/>

    <application
        android:theme="@style/UnityThemeSelector"
        android:icon="@drawable/app_icon"
        android:label="@string/app_name"
        android:debuggable="true">
        <activity android:name="com.unity3d.player.UnityPlayerActivity"
                  android:label="@string/app_name"
                  android:hardwareAccelerated="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
            <meta-data android:name="unityplayer.UnityActivity" android:value="true" />
        </activity>
        <activity android:name="com.onevcat.uniwebview.UniWebViewFileChooserActivity" />
    </application>

    <uses-permission android:name="android.permission.INTERNET" />
</manifest>
*/