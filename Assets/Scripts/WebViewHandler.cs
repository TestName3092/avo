﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class WebViewHandler : MonoBehaviour {


	#if UNITY_IPHONE
	[DllImport ("__Internal")] public static extern void _OpenURL(string url);
	#endif
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void loadURL(string url)
	{

			#if UNITY_IPHONE
			_OpenURL (url);
			#endif
		
			#if UNITY_ANDROID

			var plugin = new AndroidJavaClass("kog.binexsolutions.com.mylibrary.PluginClass");
			plugin.CallStatic("loadURL",url);

			#endif
		


	}
}
